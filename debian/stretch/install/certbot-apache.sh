#!/usr/bin/env bash

set -e

echo 'deb http://archive.debian.org/debian stretch-backports main' >> /etc/apt/sources.list
apt-get update

apt-get install -y  python-certbot-apache -t stretch-backports
