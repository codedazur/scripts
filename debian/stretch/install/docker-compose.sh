#!/usr/bin/env bash

set -e

curl -L https://github.com/docker/compose/releases/download/1.20.0/docker-compose-`uname -s`-`uname -m` > /tmp/docker-compose
install /tmp/docker-compose /usr/local/bin/docker-compose

docker-compose -v
