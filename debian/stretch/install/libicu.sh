#!/usr/bin/env bash

set -e

apt-get install -y zlib1g-dev libicu-dev g++
