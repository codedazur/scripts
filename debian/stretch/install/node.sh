#!/usr/bin/env bash
version=${1:-8.x}

set -e

# Add apt preferences for node packages
printf "Package: *\nPin: origin deb.nodesource.com\nPin-Priority: 600" > /etc/apt/preferences.d/nodesource

curl -sL https://deb.nodesource.com/setup_$version | bash -
apt-get install -y nodejs

node -v
npm -v
