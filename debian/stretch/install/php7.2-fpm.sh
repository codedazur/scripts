#!/usr/bin/env bash

set -e

apt-get install -y apt-transport-https lsb-release ca-certificates
curl -L https://packages.sury.org/php/apt.gpg -o /etc/apt/trusted.gpg.d/php.gpg
sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
apt-get update

apt-get install -y \
    php7.2 \
	php7.2-common \
	php7.2-fpm \
	php7.2-mysql \
	php7.2-pdo \
	php7.2-mbstring \
	php7.2-tokenizer \
	php7.2-xml \
	php7.2-ctype \
	php7.2-json
